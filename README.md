***Mini Project (Scientific Computing 2021)***

> _By Reine Matar_



The project is about **Reservation In A Restaurant**. 
The notebook is **interactive** with the user, where I also **collect data** using a json file. Then using **pandas** I do some analysis by plotting using **matplotlib** and other imported libraries.
